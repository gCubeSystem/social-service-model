package org.gcube.social_networking.socialnetworking.model.beans;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The user profile
 * @author Costantino Perciante at ISTI-CNR (costantino.perciante@isti.cnr.it)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserProfile {
	
	@JsonProperty("username")
	private String username;

	@JsonProperty("roles")
	private List<String> roles;

	@JsonProperty("avatar")
	private String avatar;

	@JsonProperty("fullname")
	private String fullname;

	public UserProfile() {
		super();
	}

	/**
	 * @param username
	 * @param roles
	 * @param avatar
	 * @param fullname
	 */
	public UserProfile(String username, List<String> roles, String avatar,
			String fullname) {
		super();
		this.username = username;
		this.roles = roles;
		this.avatar = avatar;
		this.fullname = fullname;
	}

	public String getUsername ()
	{
		return username;
	}

	public void setUsername (String username)
	{
		this.username = username;
	}

	public List<String> getRoles ()
	{
		return roles;
	}

	public void setRoles (List<String> roles)
	{
		this.roles = roles;
	}

	public String getAvatar ()
	{
		return avatar;
	}

	public void setAvatar (String avatar)
	{
		this.avatar = avatar;
	}

	public String getFullname ()
	{
		return fullname;
	}

	public void setFullname (String fullname)
	{
		this.fullname = fullname;
	}

	@Override
	public String toString()
	{
		return "UserProfile [username = "+username+", roles = "+roles+", avatar = "+avatar+", fullname = "+fullname+"]";
	}
}
