package org.gcube.social_networking.socialnetworking.model.beans.workspace;

import java.util.Arrays;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonTypeName("ITEM_DELETE")
public class DeletedItemEvent extends WorkspaceEvent {
	private static final WorkspaceEventType TYPE = WorkspaceEventType.ITEM_DELETE;

	@JsonProperty("itemName")
	@NotNull
	private String itemName; 	
	
	@JsonProperty("folderItem")
	@NotNull(message="folderItem cannot be missing")
	private FolderBean folder;

	public DeletedItemEvent() {
		super(TYPE);
		// TODO Auto-generated constructor stub
	}
	/**
	 * 
	 * @param idsToNotify usernames or contexts
	 * @param idsAsGroup true if idsToNotify are groups (members of contexts)
	 * @param itemName the deleted item name
	 * @param FolderBean the folder
	 */
	public DeletedItemEvent(String[] idsToNotify, boolean idsAsGroup, String itemName, FolderBean folder) {
		super(TYPE);
		this.idsToNotify = idsToNotify;
		this.idsAsGroup = idsAsGroup;
		this.itemName = itemName;
		this.folder = folder;
	}


	/**
	 * 
	 * @param idsToNotify usernames 
	 * @param itemName the deleted item name
	 * @param FolderBean the folder
	 */
	public DeletedItemEvent(String[] idsToNotify, String itemName, FolderBean folder) {
		super(TYPE);
		this.idsToNotify = idsToNotify;
		this.itemName = itemName;
		this.folder = folder;
	}


	public String getItemName() {
		return itemName;
	}


	public void setItemName(String itemName) {
		this.itemName = itemName;
	}


	public FolderBean getFolder() {
		return folder;
	}


	public void setFolder(FolderBean folder) {
		this.folder = folder;
	}


	@Override
	public String toString() {
		return "DeletedItemEvent [itemName=" + itemName + ", folder=" + folder + ", TYPE=" + TYPE + ", idsToNotify="
				+ Arrays.toString(idsToNotify) + ", idsAsGroup=" + idsAsGroup + "]";
	}
}
