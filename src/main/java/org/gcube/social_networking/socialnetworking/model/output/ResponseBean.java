package org.gcube.social_networking.socialnetworking.model.output;

import java.io.Serializable;

/**
 * Response bean with real result of type T.
 * @author Costantino Perciante at ISTI-CNR 
 * (costantino.perciante@isti.cnr.it)
 */
public class ResponseBean <T> implements Serializable {

	private static final long serialVersionUID = -2725238162673879658L;

	private boolean success;
	private String message;
	private T result;

	public ResponseBean() {
		super();
	}

	/**
	 * @param success
	 * @param message
	 * @param result
	 */
	public ResponseBean(boolean success, String message, T result) {
		super();
		this.success = success;
		this.message = message;
		this.result = result;
	}


	public boolean isSuccess() {
		return success;
	}


	public void setSuccess(boolean success) {
		this.success = success;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public T getResult() {
		return result;
	}


	public void setResult(T result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "ResponseBean [success=" + success
				+ ", message=" + message + ", result=" + result + "]";
	}
}
