package org.gcube.social_networking.socialnetworking.model.beans;

import java.util.Arrays;
/**
 * Used for serialization 
 */
public class MessageInbox extends org.gcube.common.storagehub.model.messages.Message {
	
	//needed for serialization
	public MessageInbox() {
		super();
	}

	@Override
	public String toString() {
		String body = (getBody() != null && getBody().length() > 10) ? getBody().substring(0, 9) + " ..." : "empty or short body";
		String usernameSender = getSender() != null ? getSender().getUserName() : "uknwnown";
		return "Message getSenderUsername()=" + usernameSender	+ ", getSubject()=" + getSubject() + ", getBody()=" + body + ", isRead()=" + isRead()
				+ ", isOpened()=" + isOpened() + ", getAddresses()=" + Arrays.toString(getAddresses())
				+ ", getCreationTime()=" + getCreationTime() + ", getId()="
				+ getId() + ", getName()=" + getName() + ", getPrimaryType()=" + getPrimaryType() + "]";
	}

}
