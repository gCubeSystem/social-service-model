package org.gcube.social_networking.socialnetworking.model.beans.workspace;

import java.util.Arrays;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;


@JsonIgnoreProperties(ignoreUnknown=true)
@JsonTypeName("FOLDER_ADDEDUSER")
public class FolderAddedUserEvent extends WorkspaceEvent {
	private static final WorkspaceEventType TYPE = WorkspaceEventType.FOLDER_ADDEDUSER;
	@JsonProperty("folderItem")
	@NotNull(message="folderItem cannot be missing")
	private FolderBean folder;
	
	@JsonProperty("newAddedUserIds")
	@NotNull(message="newAddedUserIds cannot be missing")
	private List<String> newAddedUserIds;
	
	public FolderAddedUserEvent() {
		super(TYPE);
	}
	/**
	 * 
	 * @param idsToNotify usernames
	 * @param idsAsGroup true if idsToNotify are groups (members of contexts)
	 * @param FolderBean the folder
	 * @param newAddedUserIds list of usernames
	 */
	public FolderAddedUserEvent(String[] idsToNotify, boolean idsAsGroup, FolderBean folder, List<String> newAddedUserIds) {
		super(TYPE);
		this.idsToNotify = idsToNotify;
		this.idsAsGroup = idsAsGroup;
		this.newAddedUserIds = newAddedUserIds;
		this.folder = folder;
	}
	/**
	 * 
	 * @param idsToNotify usernames
	 * @param FolderBean the folder
	 */
	public FolderAddedUserEvent(String[] idsToNotify, FolderBean folder, List<String> newAddedUserIds) {
		super(TYPE);
		this.idsToNotify = idsToNotify;
		this.newAddedUserIds = newAddedUserIds;
		this.folder = folder;
	}
	
	public FolderBean getFolder() {
		return folder;
	}
	
	public void setFolder(FolderBean folder) {
		this.folder = folder;
	}
	
	public List<String> getNewAddedUserIds() {
		return newAddedUserIds;
	}
	
	public void setNewAddedUserIds(List<String> newAddedUserIds) {
		this.newAddedUserIds = newAddedUserIds;
	}
	
	@Override
	public String toString() {
		return "FolderAddedUserEvent [folder=" + folder + ", newAddedUserIds=" + newAddedUserIds + ", TYPE=" + TYPE
				+ ", idsToNotify=" + Arrays.toString(idsToNotify) + ", idsAsGroup=" + idsAsGroup + "]";
	}	

}
