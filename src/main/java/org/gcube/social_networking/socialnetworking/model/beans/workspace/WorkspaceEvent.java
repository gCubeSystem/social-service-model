package org.gcube.social_networking.socialnetworking.model.beans.workspace;

import javax.validation.constraints.NotNull;
import javax.ws.rs.DefaultValue;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

/**
 * The CatalogueEvent super class
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, 
include = As.PROPERTY, property = "type") @JsonSubTypes({   
	@JsonSubTypes.Type(value = SharedFolderEvent.class, name = "FOLDER_SHARE"),
	@JsonSubTypes.Type(value = UnsharedFolderEvent.class, name = "FOLDER_UNSHARE"),
	@JsonSubTypes.Type(value = RenamedFolderEvent.class, name = "FOLDER_RENAME"),
	@JsonSubTypes.Type(value = AddedItemEvent.class, name = "ITEM_NEW"),
	@JsonSubTypes.Type(value = UpdatedItemEvent.class, name = "ITEM_UPDATE"),
	@JsonSubTypes.Type(value = DeletedItemEvent.class, name = "ITEM_DELETE"),
	@JsonSubTypes.Type(value = FolderAddedUserEvent.class, name = "FOLDER_ADDEDUSER"),
	@JsonSubTypes.Type(value = FolderRemovedUserEvent.class, name = "FOLDER_REMOVEDUSER"),		
	@JsonSubTypes.Type(value = FolderAdminUpgradeEvent.class, name = "FOLDER_ADMIN_UPGRADE"),
	@JsonSubTypes.Type(value = FolderAdminDowngradeEvent.class, name = "FOLDER_ADMIN_DOWNGRADE")	
})
public abstract class WorkspaceEvent {
	protected final WorkspaceEventType TYPE;

	/**
	 * the username of the user you wish to notify
	 */
	@JsonProperty("idsToNotify")
	@NotNull(message="recipients cannot be missing, use usernames or contexts")
	protected String[] idsToNotify;	

	/**
	 * optionl field, set to true if the idsToNotify are contexts, default is false
	 */
	@JsonProperty("idsAsGroup")
	@DefaultValue("false")
	protected boolean idsAsGroup = false;

	/**
	 * @documentationExample FOLDER_SHARE
	 * @param TYPE
	 */
	WorkspaceEvent(WorkspaceEventType TYPE) {
		this.TYPE = TYPE;
	}

	/**
	 * @param tYPE
	 * @param idsToNotify usernames or contexts
	 * @param idsAsGroup true whether the idsToNotify have to be interpreted as groups and not usernames, false is default
	 */
	WorkspaceEvent(WorkspaceEventType tYPE, String[] idsToNotify, boolean idsAsGroup) {
		super();
		TYPE = tYPE;
		this.idsToNotify = idsToNotify;
		this.idsAsGroup = idsAsGroup;
	}
	public WorkspaceEventType getType() {
		return TYPE;
	}

	public String[] getIdsToNotify() {
		return idsToNotify;
	}

	public void setIdsToNotify(String[] idsToNotify) {
		this.idsToNotify = idsToNotify;
	}
	/**
	 * 
	 * @return true whether the idsToNotify have to be interpreted as groups and not usernames
	 */
	public boolean idsAsGroup() {
		return idsAsGroup;
	}
	/**
	 * 
	 * @param idsAsGroup set true whether the idsToNotify have to be interpreted as groups and not usernames, false otherwise
	 */
	public void setIdsAsGroup(boolean idsAsGroup) {
		this.idsAsGroup = idsAsGroup;
	}

}
