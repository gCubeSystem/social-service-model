package org.gcube.social_networking.socialnetworking.model.beans;

import java.io.Serializable;
import java.util.ArrayList;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Generic input bean for methods that allow to write messages
 */
@JsonIgnoreProperties(ignoreUnknown = true) // ignore in serialization/deserialization

public class MessageInputBean implements Serializable {

	private static final long serialVersionUID = -1317811686036127456L;

	@JsonProperty("body")
	@NotNull(message="body cannot be missing")
	@Size(min=1, message="body cannot be empty")

	private String body;

	@JsonProperty("subject")
	@NotNull(message="subject cannot be missing")
	@Size(min=1, message="subject cannot be empty")
	/**
	 * subject 
	 */
	private String subject;

	@JsonProperty("recipients")
	@NotNull(message="recipients cannot be missing")
	@Size(min=1, message="at least a recipient is needed")
	@Valid // validate recursively
	private ArrayList<Recipient> recipients;
	
	/**
	 * a list of workspace item id valid in the workspace of the sender
	 */
	@JsonProperty("attachmentIds")
	@Valid // validate recursively
	private ArrayList<String> attachmentIds;

	public MessageInputBean() {
		super();
	}

	public MessageInputBean(String body, String subject,
			ArrayList<Recipient> recipients) {
		super();
		this.body = body;
		this.subject = subject;
		this.recipients = recipients;
		this.attachmentIds = new ArrayList<>();
	}
	
	public MessageInputBean(String body, String subject,
			ArrayList<Recipient> recipients, ArrayList<String> attachmentIds) {
		super();
		this.body = body;
		this.subject = subject;
		this.recipients = recipients;
		this.attachmentIds = attachmentIds;
	}
	
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public ArrayList<Recipient> getRecipients() {
		return recipients;
	}
	public void setRecipients(ArrayList<Recipient> recipients) {
		this.recipients = recipients;
	}

	public ArrayList<String> getAttachmentIds() {
		return attachmentIds;
	}

	public void setAttachmentIds(ArrayList<String> attachmentIds) {
		this.attachmentIds = attachmentIds;
	}

	@Override
	public String toString() {
		return "MessageInputBean [body=" + body + ", subject=" + subject + ", recipients=" + recipients
				+ ", attachmentIds=" + attachmentIds + "]";
	}
}
