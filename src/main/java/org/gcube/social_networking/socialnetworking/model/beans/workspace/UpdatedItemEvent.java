package org.gcube.social_networking.socialnetworking.model.beans.workspace;

import java.util.Arrays;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonTypeName("ITEM_UPDATE")
public class UpdatedItemEvent extends WorkspaceEvent {
	private static final WorkspaceEventType TYPE = WorkspaceEventType.ITEM_UPDATE;


	@JsonProperty("fileItem")
	@NotNull(message="fileItem cannot be missing")
	private FileItemBean item;

	public UpdatedItemEvent() {
		super(TYPE);
	}


	/**
	 * 
	 * @param idsToNotify usernames or contexts
	 * @param idsAsGroup true if idsToNotify are groups (members of contexts)
	 * @param item
	 */
	public UpdatedItemEvent(String[] idsToNotify, boolean idsAsGroup, FileItemBean item) {
		super(TYPE);
		this.idsToNotify = idsToNotify;
		this.idsAsGroup = idsAsGroup;
		this.item = item;
	}


	/**
	 * 
	 * @param idsToNotify usernames 
	 * @param item
	 */
	public UpdatedItemEvent(String[] idsToNotify, FileItemBean item) {
		super(TYPE);
		this.idsToNotify = idsToNotify;
		this.item = item;
	}



	public FileItemBean getItem() {
		return item;
	}

	public void setItem(FileItemBean item) {
		this.item = item;
	}


	@Override
	public String toString() {
		return "UpdatedItemEvent [item=" + item + ", TYPE=" + TYPE + ", idsToNotify=" + Arrays.toString(idsToNotify)
				+ ", idsAsGroup=" + idsAsGroup + "]";
	}
}
