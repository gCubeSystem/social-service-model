package org.gcube.social_networking.socialnetworking.model.beans.workspace;

import java.util.Arrays;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;


@JsonIgnoreProperties(ignoreUnknown=true)
@JsonTypeName("FOLDER_SHARE")
public class SharedFolderEvent extends WorkspaceEvent {
	private static final WorkspaceEventType TYPE = WorkspaceEventType.FOLDER_SHARE;
	@JsonProperty("folderItem")
	@NotNull(message="folderItem cannot be missing")
	private FolderBean folder;
	
	public SharedFolderEvent() {
		super(TYPE);
	}
	/**
	 * 
	 * @param idsToNotify usernames
	 * @param idsAsGroup true if idsToNotify are groups (members of contexts)
	 * @param FolderBean the folder
	 */
	public SharedFolderEvent(String[] idsToNotify, boolean idsAsGroup, FolderBean folder) {
		super(TYPE);
		this.idsToNotify = idsToNotify;
		this.idsAsGroup = idsAsGroup;
		this.folder = folder;
	}
	/**
	 * 
	 * @param idsToNotify usernames
	 * @param FolderBean the folder
	 */
	public SharedFolderEvent(String[] idsToNotify, FolderBean folder) {
		super(TYPE);
		this.idsToNotify = idsToNotify;
		this.folder = folder;
	}



	public FolderBean getFolder() {
		return folder;
	}

	public void setFolder(FolderBean folder) {
		this.folder = folder;
	}
	@Override
	public String toString() {
		return "SharedFolderEvent [folder=" + folder + ", TYPE=" + TYPE + ", idsToNotify="
				+ Arrays.toString(idsToNotify) + ", idsAsGroup=" + idsAsGroup + "]";
	}


}
