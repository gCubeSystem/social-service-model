package org.gcube.social_networking.socialnetworking.model.beans.catalogue;

import java.net.URL;
import java.util.Arrays;

import javax.validation.constraints.NotNull;
import javax.ws.rs.DefaultValue;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The CatalogueEvent super class
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CatalogueEvent {
	protected CatalogueEventType type;

	/**
	 * the username of the user you wish to notify
	 */
	@JsonProperty("idsToNotify")
	@NotNull(message="recipients cannot be missing, use usernames or contexts")
	protected String[] idsToNotify;	

	/**
	 * the item identifier or name (it is used in the subject of the email)
	 */
	@JsonProperty("itemId")
	@NotNull(message="itemId cannot be missing")
	protected String itemId;
	/**
	 * the text that you want to write in the notification (text/plain format)
	 */
	@JsonProperty("notifyText")
	@NotNull(message="notifyText cannot be missing")
	protected String notifyText;
	/**
	 * the URL to redirect when clicking on the notification
	 */
	@JsonProperty("itemURL")
	@NotNull(message="itemURL cannot be missing, use catalogue resolver URL")
	protected URL itemURL;

	/**
	 * optionl field, set to true if the idsToNotify are contexts, default is false
	 */
	@JsonProperty("idsAsGroup")
	@DefaultValue("false")
	protected boolean idsAsGroup = false;

	public CatalogueEvent() {
	}

	

	/**
	 * for sending to users or groups
	 * @param type
	 * @param idsToNotify
	 * @param itemId
	 * @param notifyText
	 * @param itemURL
	 * @param idsAsGroup
	 */
	public CatalogueEvent(CatalogueEventType type, String[] idsToNotify, String itemId, String notifyText, URL itemURL,
			boolean idsAsGroup) {
		super();
		this.type = type;
		this.idsToNotify = idsToNotify;
		this.itemId = itemId;
		this.notifyText = notifyText;
		this.itemURL = itemURL;
		this.idsAsGroup = idsAsGroup;
	}
	/**
	 * for sending to users
	 * @param type
	 * @param idsToNotify
	 * @param itemId
	 * @param notifyText
	 * @param itemURL
	 */
	public CatalogueEvent(CatalogueEventType type, String[] idsToNotify, String itemId, String notifyText, URL itemURL) {
		super();
		this.type = type;
		this.idsToNotify = idsToNotify;
		this.itemId = itemId;
		this.notifyText = notifyText;
		this.itemURL = itemURL;
		this.idsAsGroup = false;
	}



	public CatalogueEventType getType() {
		return type;
	}



	public void setType(CatalogueEventType type) {
		this.type = type;
	}



	public String[] getIdsToNotify() {
		return idsToNotify;
	}

	public void setIdsToNotify(String[] idsToNotify) {
		this.idsToNotify = idsToNotify;
	}
	/**
	 * 
	 * @return true whether the idsToNotify have to be interpreted as groups and not usernames
	 */
	public boolean idsAsGroup() {
		return idsAsGroup;
	}
	/**
	 * 
	 * @param idsAsGroup set true whether the idsToNotify have to be interpreted as groups and not usernames, false otherwise
	 */
	public void setIdsAsGroup(boolean idsAsGroup) {
		this.idsAsGroup = idsAsGroup;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getNotifyText() {
		return notifyText;
	}

	public void setNotifyText(String notifyText) {
		this.notifyText = notifyText;
	}

	public URL getItemURL() {
		return itemURL;
	}

	public void setItemURL(URL itemURL) {
		this.itemURL = itemURL;
	}

	public boolean isIdsAsGroup() {
		return idsAsGroup;
	}



	@Override
	public String toString() {
		return "CatalogueEvent [type=" + type + ", idsToNotify=" + Arrays.toString(idsToNotify) + ", itemId=" + itemId
				+ ", notifyText=" + notifyText + ", itemURL=" + itemURL + ", idsAsGroup=" + idsAsGroup + "]";
	}

	
}
