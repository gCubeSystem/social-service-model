package org.gcube.social_networking.socialnetworking.model.beans.workspace;

import java.util.Arrays;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonTypeName("FOLDER_UNSHARE")
public class UnsharedFolderEvent extends WorkspaceEvent {

	private static final WorkspaceEventType TYPE = WorkspaceEventType.FOLDER_UNSHARE;
	
	@JsonProperty("unsharedFolderId")
	@NotNull(message="folderid cannot be missing")
	private String unsharedFolderId;
	
	@JsonProperty("unsharedFolderName")
	@NotNull(message="unsharedFolderName cannot be missing")
	private String unsharedFolderName;	


	public UnsharedFolderEvent() {
		super(TYPE);
	}

	/**
	 * 
	 * @param idsToNotify usernames or contexts
	 * @param idsAsGroup true if idsToNotify are groups (members of contexts)
	 * @param unsharedFolderId
	 * @param unsharedFolderName
	 */
	public UnsharedFolderEvent(String[] idsToNotify, boolean idsAsGroup, String unsharedFolderId, String unsharedFolderName) {
		super(TYPE);
		this.idsToNotify = idsToNotify;
		this.idsAsGroup = idsAsGroup;
		this.unsharedFolderId = unsharedFolderId;
		this.unsharedFolderName = unsharedFolderName;
	}
	/**
	 * 
	 * @param idsToNotify usernames
	 * @param unsharedFolderId
	 * @param unsharedFolderName
	 */
	public UnsharedFolderEvent(String[] idsToNotify, String unsharedFolderId, String unsharedFolderName) {
		super(TYPE);
		this.idsToNotify = idsToNotify;
		this.unsharedFolderId = unsharedFolderId;
		this.unsharedFolderName = unsharedFolderName;
	}



	public String getUnsharedFolderId() {
		return unsharedFolderId;
	}


	public void setUnsharedFolderId(String unsharedFolderId) {
		this.unsharedFolderId = unsharedFolderId;
	}

	public String getUnsharedFolderName() {
		return unsharedFolderName;
	}

	public void setUnsharedFolderName(String unsharedFolderName) {
		this.unsharedFolderName = unsharedFolderName;
	}

	@Override
	public String toString() {
		return "UnsharedFolderEvent [unsharedFolderId=" + unsharedFolderId + ", unsharedFolderName="
				+ unsharedFolderName + ", TYPE=" + TYPE + ", idsToNotify=" + Arrays.toString(idsToNotify)
				+ ", idsAsGroup=" + idsAsGroup + "]";
	}


	
}
	