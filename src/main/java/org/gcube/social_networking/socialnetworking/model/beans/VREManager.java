package org.gcube.social_networking.socialnetworking.model.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class VREManager {

	private String username;
	private String fullname;

	public VREManager() {
		super();
	}

	/**
	 * @param username
	 * @param fullname
	 */
	public VREManager(String username, String fullname) {
		super();
		this.username = username;
		this.fullname = fullname;
	}

	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	@Override
	public String toString() {
		return "VREManager [username=" + username + ", fullname=" + fullname
				+ "]";
	}

}
