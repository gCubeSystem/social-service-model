package org.gcube.social_networking.socialnetworking.model.beans.catalogue;

public enum CatalogueEventType {
	/**
	 * Someone publishes a new item
	 */
	ITEM_PUBLISHED,
	/**
	 * Someone submits an item for consideration (only for catalogue moderators)
	 */
	ITEM_SUBMITTED,
	/**
	 * Someone rejects a submitted item  (only for item creators and catalogue moderators)
	 */
	ITEM_REJECTED,
	/**
	 *  Someone removes an item
	 */
	ITEM_REMOVED,
	/**
	 * Someone updates an item 
	 */
	ITEM_UPDATED;

	
}
