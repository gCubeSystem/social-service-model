package org.gcube.social_networking.socialnetworking.model.beans.workspace;

import java.util.Arrays;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonTypeName("ITEM_NEW")
public class AddedItemEvent extends WorkspaceEvent {
	private static final WorkspaceEventType TYPE = WorkspaceEventType.ITEM_NEW;


	@JsonProperty("fileItem")
	@NotNull(message="fileItem cannot be missing")
	private FileItemBean item;

	public AddedItemEvent() {
		super(TYPE);
	}


	/**
	 * 
	 * @param idsToNotify usernames or contexts
	 * @param idsAsGroup true if idsToNotify are groups (members of contexts)
	 * @param item
	 */
	public AddedItemEvent(String[] idsToNotify, boolean idsAsGroup, FileItemBean item) {
		super(TYPE);
		this.idsToNotify = idsToNotify;
		this.idsAsGroup = idsAsGroup;
		this.item = item;
	}


	/**
	 * 
	 * @param idsToNotify usernames 
	 * @param item
	 */
	public AddedItemEvent(String[] idsToNotify, FileItemBean item) {
		super(TYPE);
		this.idsToNotify = idsToNotify;
		this.item = item;
	}



	public FileItemBean getItem() {
		return item;
	}

	public void setItem(FileItemBean item) {
		this.item = item;
	}


	@Override
	public String toString() {
		return "PublishedItemEvent [item=" + item + ", TYPE=" + TYPE + ", idsToNotify=" + Arrays.toString(idsToNotify)
				+ ", idsAsGroup=" + idsAsGroup + "]";
	}

}
