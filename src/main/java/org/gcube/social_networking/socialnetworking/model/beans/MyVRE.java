package org.gcube.social_networking.socialnetworking.model.beans;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MyVRE
{
	private String description;

	private String name;

	private String context;

	private List<VREManager> managers;

	/**
	 * 
	 */
	 public MyVRE() {
		 super();
	 }

	 /**
	  * @param description
	  * @param name
	  * @param context
	  */
	 public MyVRE(String description, String name, String context) {
		 super();
		 this.description = description;
		 this.name = name;
		 this.context = context;
	 }

	 public String getDescription ()
	 {
		 return description;
	 }

	 public void setDescription (String description)
	 {
		 this.description = description;
	 }

	 public String getName ()
	 {
		 return name;
	 }

	 public void setName (String name)
	 {
		 this.name = name;
	 }

	 public String getContext ()
	 {
		 return context;
	 }

	 public void setContext (String context)
	 {
		 this.context = context;
	 }

	 public List<VREManager> getManagers() {
		 return managers;
	 }

	 public void setManagers(List<VREManager> managers) {
		 this.managers = managers;
	 }

	 @Override
	 public String toString() {
		 return "MyVRE [description=" + description + ", name=" + name
				 + ", context=" + context + ", managers=" + managers + "]";
	 }
}

