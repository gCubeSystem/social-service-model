package org.gcube.social_networking.socialnetworking.model.beans.workspace;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FileItemBean {

	@JsonProperty("id")
	@NotNull
	private String id;
	
	@JsonProperty("name")
	@NotNull
	private String name; 
	
	@JsonProperty("title")
	@NotNull
	private String title; 
	
	@JsonProperty("path")
	@NotNull
	private String path;
	
	@JsonProperty("parent")
	@NotNull
	private FolderBean parent;
	
	public FileItemBean() {
		super();
	}
	/**
	 * @param id
	 * @param name
	 * @param title
	 * @param path
	 * @param parent
	 */
	public FileItemBean(String id, String name, String title, String path, FolderBean parent) {
		super();
		this.id = id;
		this.name = name;
		this.title = title;
		this.path = path;
		this.parent = parent;
	}


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public FolderBean getParent() {
		return parent;
	}

	public void setParent(FolderBean parent) {
		this.parent = parent;
	}

	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FileItemBean [id=");
		builder.append(id);
		builder.append(", name=");
		builder.append(name);
		builder.append(", title=");
		builder.append(title);
		builder.append(", path=");
		builder.append(path);
		builder.append(", parent=");
		builder.append(parent);
		builder.append("]");
		return builder.toString();
	}



}
