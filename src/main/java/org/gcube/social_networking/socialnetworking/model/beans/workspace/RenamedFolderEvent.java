package org.gcube.social_networking.socialnetworking.model.beans.workspace;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonTypeName("FOLDER_RENAME")
public class RenamedFolderEvent extends WorkspaceEvent {
	private static final WorkspaceEventType TYPE = WorkspaceEventType.FOLDER_RENAME;
	
	@JsonProperty("previousName")
	@NotNull(message="previousName cannot be missing")
	private String previousName;
	
	@JsonProperty("newName")
	@NotNull(message="newName cannot be missing")
	private String newName;	
	
	@JsonProperty("renamedFolderId")
	@NotNull(message="renamedFolderId cannot be missing")
	private String renamedFolderId;	


	public RenamedFolderEvent() {
		super(TYPE);
	}

	/**
	 * @param idsToNotify usernames or contexts
	 * @param idsAsGroup true if idsToNotify are groups (members of contexts)
	 * @param previousName
	 * @param newName
	 * @param renamedFolderId
	 */
	public RenamedFolderEvent(String[] idsToNotify, boolean idsAsGroup, String previousName, String newName, String renamedFolderId) {
		super(TYPE);
		this.idsToNotify = idsToNotify;
		this.idsAsGroup = idsAsGroup;
		this.previousName = previousName;
		this.newName = newName;
		this.renamedFolderId = renamedFolderId;
	}

	/**
	 * @param idsToNotify usernames 
	 * @param previousName
	 * @param newName
	 * @param renamedFolderId
	 */
	public RenamedFolderEvent(String[] idsToNotify, String previousName, String newName, String renamedFolderId) {
		super(TYPE);
		this.idsToNotify = idsToNotify;
		this.previousName = previousName;
		this.newName = newName;
		this.renamedFolderId = renamedFolderId;
	}


	public String getPreviousName() {
		return previousName;
	}


	public void setPreviousName(String previousName) {
		this.previousName = previousName;
	}


	public String getNewName() {
		return newName;
	}


	public void setNewName(String newName) {
		this.newName = newName;
	}


	public String getRenamedFolderId() {
		return renamedFolderId;
	}


	public void setRenamedFolderId(String renamedFolderId) {
		this.renamedFolderId = renamedFolderId;
	}


	@Override
	public String toString() {
		return "RenamedFolderEvent [previousName=" + previousName + ", newName=" + newName + ", renamedFolderId="
				+ renamedFolderId + "]";
	}
	
}
	