package org.gcube.social_networking.socialnetworking.model.beans.workspace;

public enum WorkspaceEventType {

	/**
	 * use to notify a user she got a workspace item new in some of her workspace shared folder
	 */
	ITEM_NEW(AddedItemEvent.class),
	/**
	 * use to notify a user she got a workspace item deleted from one of his workspace shared folder
	 */
	ITEM_DELETE(DeletedItemEvent.class),
	/**
	 * use to notify a user he got a workspace item updated from one of his workspace shared folder
	 */
	ITEM_UPDATE(UpdatedItemEvent.class),
	/**
	 * use to notify a user he got a workspace folder shared
	 */
	FOLDER_SHARE(SharedFolderEvent.class),
	/**
	 * use to notify a workspace folder Unshared
	 */
	FOLDER_UNSHARE(UnsharedFolderEvent.class),
	/**
	 * use to notify a workspace folder renamed
	 */
	FOLDER_RENAME(RenamedFolderEvent.class),
	/**
	 * use to notify a user that he got upgraded to administrator of a shared folder
	 */
	FOLDER_ADMIN_UPGRADE(FolderAdminUpgradeEvent.class),
	/**
	 * use to notify a user that he got downgraded from being administrator of a shared folder
	 */
	FOLDER_ADMIN_DOWNGRADE(FolderAdminDowngradeEvent.class),
	/**
	 * use to notify that new user(s) were added in on of his workspace shared folder
	 */
	FOLDER_ADDEDUSER(FolderAddedUserEvent.class),
	/**
	 * use to notify user(s) that they were removed from a workspace shared folder
	 */
	FOLDER_REMOVEDUSER(FolderRemovedUserEvent.class);
	

	
	
	
	Class<? extends WorkspaceEvent> beanClass;
	
	WorkspaceEventType(Class<? extends WorkspaceEvent> beanClass){
		this.beanClass = beanClass;
	}

	
	public Class<? extends WorkspaceEvent> getNotificationClass(){
		return this.beanClass;
	}
	
}
