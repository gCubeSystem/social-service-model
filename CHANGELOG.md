# Changelog

## [v1.3.0] - 2024-10-25

- Feature #27999 [StorageHub] downstream components to upgrade in order to work with storagehub 1.5.0
- moved to gcube-smartgears-bom 2.5.1
- removed gcube-bom dependency

## [v1.2.0] - 2022-05-02

- Feature #23995 dd support for set Message read / unread

## [v1.1.7] - 2022-05-02

 - Added support for workspace and catalogue notifications

## [v1.0.0] - 2022-04-05

 - First release


This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).